# coding=utf-8
import requests

# Ara a més importarem la llibreria time que té funcions com sleep per
# fer que esperi un cert temps el nostre programa

import time

# Definirem una variable iteració de tipus entera que tindrà com a valor
# inicial 0 i que s'anirà incrementant en el bucle while. Aquest bucle
# finalitzarà quan iteració sigui més gran que 10.

# Guardarem totes les dades que ens vagi retornant el web en una llista
# anomenada databicing. El sistema operatiu serà l'encarregat de trobar
# un lloc a la memòria RAM de l'ordinador on desar-les
databicing=[]
iteracio=0
while iteracio <= 10:
	databicing.append(requests.get("https://www.bicing.cat/current-bikes-in-use.json").json())
	time.sleep(1)
	iteracio=iteracio+1
	print("Petició Nº:"+str(iteracio))
